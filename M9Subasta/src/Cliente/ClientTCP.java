package Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.EmptyStackException;

import javax.swing.JOptionPane;

import Clases.Comprador;
import Server.ConexionServerACliente;
import Server.ServerTCP;

public class ClientTCP extends Thread {

	Socket socket = null;
	PrintWriter out = null;
	BufferedReader in = null;
	public static final String HOSTNAME = "localhost";
	public static final int PORT = 20000;
	private static boolean acabar = false;
	public Comprador comprador = null;

	public ConexionClienteAServer sender = null;
	BufferedReader read;
	PrintWriter output;
	private String username;
	private String saldo;
	public boolean aceptar = true;
	public boolean logueado = false;

	public static void main(String[] args) {
		ClientTCP client = new ClientTCP();
		client.start();
	}

	@Override
	public void run() {
		try {
			startClient();
			startSender();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (!acabar) {
			// seguira ejecutando el cliente
			leerMensajes();
		}
	}

	public static boolean isParsable(String input) {
		boolean parsable = true;
		try {
			Integer.parseInt(input);
		} catch (NumberFormatException e) {
			parsable = false;
		}
		return parsable;
	}

	private void leerMensajes() {
		try {
			if (logueado) {
				String mensaje = read.readLine();
				if (isParsable(mensaje)) {
					System.out.println("Tu bolsillo ahora tiene: " + mensaje);
				} else {
					System.out.println(mensaje);
				}

			} else {
				Thread.sleep(1000);
			}
		} catch (IOException e) {
			acabar = true;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void startSender() {
		sender = new ConexionClienteAServer(socket);
		// comprador = new Comprador(username, Integer.parseInt(saldo));

		sender.setDaemon(true);
		sender.start();

	}

	public void startClient() throws UnknownHostException, IOException {
		// Create socket connection
		socket = new Socket(HOSTNAME, PORT);

		// create printwriter for sending login to server
		output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

		String syso = " ";
		while (!syso.equals("1") && aceptar) {
			// prompt for user name
			username = JOptionPane.showInputDialog(null, "Enter User Name:");

			try {
				if (username == null || (username != null && ("".equals(username)))) {
					aceptar = false;
				} else {
					// send user name to server
					output.println(username);

					// prompt for password
					String password = JOptionPane.showInputDialog(null, "Enter Password");
					if (password == null || (password != null && ("".equals(password)))) {
						aceptar = false;
					} else {
						// send password to server
						output.println(password);
						output.flush();

						// create Buffered reader for reading response from server
						read = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						syso = read.readLine();
					}
					if (aceptar && !syso.equals("Login failed")) {
						System.out.println("Login aceptado");
						syso = "1";
					}
				}
			} catch (EmptyStackException e) {
				e.printStackTrace();
				System.out.println("salta el catch por cancelar");
			}

		}

		if (!aceptar) {
			output.println("cancelar login");
			System.exit(1);

		}
		if (aceptar) {
			// read response from server
			String respuestaSaldo = read.readLine();
			saldo = read.readLine();
			System.out.println(" Hola " + username + " su saldo es: " + saldo);
			// display response
			JOptionPane.showMessageDialog(null, respuestaSaldo + "\nSu saldo es: " + saldo);
			logueado = true;
		}

	}

	public Comprador getComprador() {
		return comprador;
	}

	public void setComprador(Comprador comprador) {
		this.comprador = comprador;
	}
}