package Cliente;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import Clases.Comprador;

public class ConexionClienteAServer extends Thread {
	private Socket socket;
	private Comprador comprador;
	private PrintWriter pw;

	public ConexionClienteAServer(Socket socket) {
		this.socket = socket;
		// this.pw = pw;
	}

	@Override
	public void run() {

		try {
			pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			enviarMensajes();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void enviarMensajes() {
		Scanner sc = new Scanner(System.in);
		String msg = "";
		while (!msg.equals("salir")) {
			msg = sc.nextLine();
			pw.println(msg);
			pw.flush();
			System.out.println("Enviado: " + msg);
		}
		sc.close();

	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public Comprador getComprador() {
		return comprador;
	}

	public void setComprador(Comprador comprador) {
		this.comprador = comprador;
	}

}
