package Server;
import java.net.ServerSocket;
import java.net.Socket;

public class DaemonGestorConexiones extends Thread {

	static ServerSocket serverSocket = null;
	static Socket socketCliente = null;
	private static boolean aceptarConexiones = true;

	DaemonGestorConexiones(ServerSocket servSocket) {
		serverSocket = servSocket;
	}

	@Override
	public void run() {
		esperarConexiones();
	}

	public void esperarConexiones() {
		try {
			while (aceptarConexiones) {
				// accept connection from client
				socketCliente = serverSocket.accept();
				ServerTCP.getListaConexiones().add(new ConexionServerACliente(socketCliente,ServerTCP.getLusuarios()));
				ServerTCP.getListaConexiones().get(ServerTCP.getListaConexiones().size() - 1).start();
				System.out.println("Waiting for connection from client");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Falla conexi�n");
			System.exit(1);
		}
	}

	public static boolean isAceptarConexiones() {
		return aceptarConexiones;
	}

	public static void setAceptarConexiones(boolean aceptarConexiones) {
		DaemonGestorConexiones.aceptarConexiones = aceptarConexiones;
	}
}
