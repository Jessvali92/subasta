package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import Clases.Comprador;
import Clases.Licitacions;
import Clases.Lot;
import Cliente.ClientTCP;

public class ServerTCP {

	static ServerSocket serverSocket = null;
	static Socket socketCliente = null;
	static BufferedReader bf;
	static PrintWriter pw;
	static ClientTCP client;
	static Lot lotSubastat;
	private static ArrayList<ConexionServerACliente> listaConexiones = new ArrayList<ConexionServerACliente>();
	private static ArrayList<ConexionServerACliente> listaConexionesLogueados = new ArrayList<ConexionServerACliente>();
	private static ArrayList<Lot> listaLots = new ArrayList<Lot>();
	private static ArrayList<Licitacions> listaLicitaciones = new ArrayList<Licitacions>();
	private static Licitacions licitacionMayor = new Licitacions();
	private static ArrayList<Comprador> lusuarios = new ArrayList<Comprador>();
	private static Connection db = null;
	private static Statement st = null;
	private static boolean fiSubasta = false;
	private static String url = "jdbc:mysql://localhost:3306/subasta";
	private static String USER = "root";
	private static String PASSWORD = "super3";
	MulticastSocket enviador;
	final static String INET_ADDR = "224.0.0.3";
	final static int PORT = 8888;
	private static Semaphore sf = new Semaphore(1, true);
	public static int tiempoDeSubasta = 30; // tiempo de espera thread (EN SEGUNDOS)
	static Timer tiempo = new Timer();
	static boolean acabarSubasta = false;
	static int contador = 0;
	static boolean recibido = false;

	/* Este m�todo se ha puesto arriba para que sea mas visible */

	static TimerTask tk = new TimerTask() {

		@Override
		public void run() {
			if (recibido) {
				contador = 0;
				recibido = false;
			} else {
				contador++;
				if (contador == tiempoDeSubasta) {
					acabarSubasta = true;
					contador = 0;
				}
			}
		}
	};

	public static void main(String[] args) {

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");
		//crearDB();
		//crearTablas();

		System.out.println("Connection Starting on port:" + ClientTCP.PORT);
		// make connection to client on port specified
		try {
			serverSocket = new ServerSocket(ClientTCP.PORT);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		DaemonLotGenerador lots = new DaemonLotGenerador();
		lots.setDaemon(true);
		lots.start();

		DaemonGestorConexiones gestorConexiones = new DaemonGestorConexiones(serverSocket);
		gestorConexiones.setDaemon(true);
		gestorConexiones.start();

		try {
			// tiempo de espera para que conectes los clientes a tiempo
			System.out.println("La subasta empezar� en 10 segundos Conecta los clientes");
			Thread.sleep(10000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tiempo.scheduleAtFixedRate(tk, 0, 1000);
		while (true) {
			String linea = "Subastando un lote del producto: " + listaLots.get(0).getIdProducte()
					+ ", precio de salida: " + listaLots.get(0).getPreuSortida() + "\nComienza la subasta";
			System.out.println(linea);
			for (ConexionServerACliente c : listaConexionesLogueados) {
				c.getPw().println(linea);
				c.getPw().flush();
			}

			try {
				while (!acabarSubasta) {
					Thread.sleep(1000);
				}
				if (licitacionMayor.getNombreComprador() != null) {
					// Thread.sleep(1000);
					sf.acquire();
					for (ConexionServerACliente c : listaConexiones) {
						c.getPw().println("Fin de la subasta\n" + "Adjudicado a : "
								+ licitacionMayor.getNombreComprador() + " por " + licitacionMayor.getImport() + "$");
						c.getPw().flush();
						licitacionMayor.setAcceptada(true);
						
					}
					sf.release();
					actualizarSaldo();
					actualizarTablaLicitaciones(true);
				} else {
					System.out.println("Nadie ha pujado por este articulo");
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			licitacionMayor = new Licitacions();
			listaLicitaciones = new ArrayList<Licitacions>();
			acabarSubasta = false;
			listaLots.remove(0);
		}
	}

	public static void actualizarTablaLicitaciones(boolean aceptada) {
		try {
			if (!aceptada) {
				String sql;
				db = DriverManager.getConnection(url, USER, PASSWORD);
				st = db.createStatement();
				for (Licitacions l : listaLicitaciones) {
					if (l.isAcceptada()) {
						sql = "INSERT INTO licitaciones (username,precio_venta,aceptado,producto) VALUES ('"
								+ l.getNombreComprador() + "', " + l.getImport() + ",'N', " + l.getIdProducte() + ");";
						st.executeUpdate(sql);
					}
				}

			} else {

				String sql;
				db = DriverManager.getConnection(url, USER, PASSWORD);
				st = db.createStatement();
				for (Licitacions l : listaLicitaciones) {
					if (l.isAcceptada()) {
						sql = "INSERT INTO licitaciones (username,precio_venta,aceptado,producto) VALUES ('"
								+ l.getNombreComprador() + "', " + l.getImport() + ",'S', " + l.getIdProducte() + ");";

						st.executeUpdate(sql);

					}

				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<Lot> getListaLots() {
		return listaLots;
	}

	public static void setListaLots(ArrayList<Lot> listaLots) {
		ServerTCP.listaLots = listaLots;
	}

	private static void crearDB() {
		try {
			String sql = "";
			db = DriverManager.getConnection("jdbc:mysql://localhost:3306/", USER, PASSWORD);
			st = db.createStatement();
			sql = "CREATE DATABASE subasta;";
			st.executeUpdate(sql);
			db = DriverManager.getConnection(url, USER, PASSWORD);
			st = db.createStatement();
			crearTablas();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Lot getLotSubastat() {
		return lotSubastat;
	}

	public static void setLotSubastat(Lot lotSubastat) {
		ServerTCP.lotSubastat = lotSubastat;
	}

	public static void actualizarSaldo() {
		String sql;
		if (listaConexiones != null) {
			for (ConexionServerACliente c : listaConexiones) {

				if (licitacionMayor.getNombreComprador() != null
						&& c.getComprador().getUser().equals(licitacionMayor.getNombreComprador())) {
					try {

						int nuevoSaldo = c.getComprador().getSaldo() - licitacionMayor.getImport();
						// actualizar el saldo del cliente
						db = DriverManager.getConnection(url, USER, PASSWORD);
						st = db.createStatement();
						sql = "UPDATE users SET saldo = " + nuevoSaldo + " WHERE username LIKE '"
								+ licitacionMayor.getNombreComprador() + "';";
						st.executeUpdate(sql);
						c.getComprador().setSaldo(nuevoSaldo);
						c.enviarSaldo();

					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else {

		}

	}

	public static void crearTablas() {
		String sql;
		try {
			db = DriverManager.getConnection(url, USER, PASSWORD);

			st = db.createStatement();

			/* crear tabla licitaciones */

			sql = "CREATE TABLE licitaciones (id INTEGER AUTO_INCREMENT NOT NULL, username VARCHAR(30), "
					+ "precio_venta INTEGER, aceptado CHAR DEFAULT 'N', producto INTEGER, PRIMARY KEY(id));";
			st.executeUpdate(sql);

			/* crear tabla usuarios */

			sql = "CREATE TABLE users (id INTEGER NOT NULL, " + "username VARCHAR(30) UNIQUE, "
					+ "password VARCHAR(30), " + "saldo INTEGER DEFAULT 2000, " + "PRIMARY KEY(id));";
			st.executeUpdate(sql);
			sql = "INSERT INTO users VALUES (1,'admin','super3',2000);";
			st.executeUpdate(sql);
			sql = "INSERT INTO users VALUES (2,'jess','super3',20000);";
			st.executeUpdate(sql);
			sql = "INSERT INTO users VALUES (3,'carlos','super3',1000);";
			st.executeUpdate(sql);

			/* crear tabla lotes */

			sql = "CREATE TABLE lots (id INTEGER AUTO_INCREMENT NOT NULL, " + "idProducte INTEGER, "
					+ "preuSortida INTEGER, " + "importAdjudicacio INTEGER, " + "idComprador INTEGER, "
					+ "PRIMARY KEY(id));";
			st.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("Ya tiene las tablas creadas");
		}
	}

	public static Connection getDb() {
		return db;
	}

	public static void setDb(Connection db) {
		ServerTCP.db = db;
	}

	public static ArrayList<ConexionServerACliente> getListaConexiones() {
		return listaConexiones;
	}

	public static void setListaConexiones(ArrayList<ConexionServerACliente> listaConexiones) {
		ServerTCP.listaConexiones = listaConexiones;
	}

	public static String getInetAddr() {
		return INET_ADDR;
	}

	public static ArrayList<Licitacions> getListaLicitaciones() {
		return listaLicitaciones;
	}

	public static void setListaLicitaciones(ArrayList<Licitacions> listaLicitaciones) {
		ServerTCP.listaLicitaciones = listaLicitaciones;
	}

	public static Licitacions getLicitacionMayor() {
		return licitacionMayor;
	}

	public static void setLicitacionMayor(Licitacions licitacionMayor) {
		ServerTCP.licitacionMayor = licitacionMayor;
	}

	public static Semaphore getSf() {
		return sf;
	}

	public static void setSf(Semaphore sf) {
		ServerTCP.sf = sf;
	}

	public static ArrayList<Comprador> getLusuarios() {
		return lusuarios;
	}

	public static void setLusuarios(ArrayList<Comprador> lusuarios) {
		ServerTCP.lusuarios = lusuarios;
	}

	public static Timer getTiempo() {
		return tiempo;
	}

	public static void setTiempo(Timer tiempo) {
		ServerTCP.tiempo = tiempo;
	}

	public static TimerTask getTk() {
		return tk;
	}

	public static void setTk(TimerTask tk) {
		ServerTCP.tk = tk;
	}
	public static ArrayList<ConexionServerACliente> getListaConexionesLogueados() {
		return listaConexionesLogueados;
	}

	public static void setListaConexionesLogueados(ArrayList<ConexionServerACliente> listaConexionesLogueados) {
		ServerTCP.listaConexionesLogueados = listaConexionesLogueados;
	}

}