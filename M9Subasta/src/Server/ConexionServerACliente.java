package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import com.google.protobuf.TextFormat.ParseException;

import Clases.Comprador;
import Clases.Licitacions;

public class ConexionServerACliente extends Thread {
	private static boolean acabar = false;
	private Socket socket;
	private Comprador comprador;
	private BufferedReader bf;
	private PrintWriter pw;
	private Connection db = null;
	private Statement st = null;
	private ResultSet rs = null;
	private String username;
	private int saldo;
	private static String url = "jdbc:mysql://localhost:3306/subasta";
	private static ArrayList<Comprador> lusuarios = new ArrayList<Comprador>();
	private boolean intentarLogin = true;
	public boolean yaLogueado = false;

	public ConexionServerACliente(Socket socket, ArrayList<Comprador> listaUsuariosServer) {
		this.socket = socket;
		lusuarios = listaUsuariosServer;
	}

	@Override
	public void run() {

		try {
			bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			// INICIO LOGIN
			while (intentarLogin) {
				yaLogueado = false;
				logInfo();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String msg = "";
		while (!msg.equals("salir") && !acabar && !intentarLogin) {
			try {
				msg = bf.readLine();

				System.out.println("Recibido: " + msg);

				if (isParsable(msg)) {
					int importe;
					importe = Integer.parseInt(msg);

					try {
						ServerTCP.getSf().acquire();
						if (ServerTCP.getListaLots().get(0).getPreuSortida() <= importe) {
							if (this.saldo >= importe) {
								Licitacions licitacion = new Licitacions(
										ServerTCP.getListaLots().get(0).getIdProducte(), comprador.getUser(), importe);
								ServerTCP.getSf().release();
								if (importe > ServerTCP.getLicitacionMayor().getImport()) {
									// reset tiempo, acaba de entrar una licitacion

									// aceptar licitacion y guardarla
									ServerTCP.setLicitacionMayor(licitacion);
									for (ConexionServerACliente c : ServerTCP.getListaConexiones()) {
										c.getPw().println(this.username + " : " + licitacion.getImport());
										c.getPw().flush();
									}
									ServerTCP.recibido = true;
									ServerTCP.getListaLicitaciones().add(licitacion);
								} else {

									pw.println("No se acepta, la apuesta mayor actual es: "
											+ ServerTCP.getLicitacionMayor().getImport());
									pw.flush();

								}
								//ServerTCP.getListaLicitaciones().add(licitacion);
							} else {
								ServerTCP.getSf().release();
								pw.println("No se acepta, su saldo es insuficiente");
								pw.flush();
							}

						} else {
							ServerTCP.getSf().release();
							pw.println("No se acepta, su apuesta es menor que la inicial");
							pw.flush();
						}
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}

			} catch (IOException e) {
				acabar = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isParsable(String input) {
		boolean parsable = true;
		try {
			Integer.parseInt(input);
		} catch (NumberFormatException e) {
			parsable = false;
		}
		return parsable;
	}

	public void logInfo() {
		// open buffered reader for reading data from client

		try {
			username = bf.readLine();
			for (ConexionServerACliente c : ServerTCP.getListaConexionesLogueados()) {
				if (c.username.equals(username)) {
					yaLogueado = true;
					intentarLogin=true;
				}

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Has cancelado el login");
			intentarLogin = false;
		}
		String password = "";
		try {
		
			password = bf.readLine();
			if (password == null) {
				System.out.println("Has cancelado el login");
				intentarLogin = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Has cancelado el login");
			intentarLogin = false;
		}
		if(!yaLogueado) {
			if (comprobarLogin(username, password) && intentarLogin && password != null && !password.equals("")) {
				// open printwriter for writing data to client
				pw.println(1);
				pw.flush();
				pw.println("Welcome, " + username);
				pw.flush();
				System.out.println("Enviado: Welcome, " + username);
				enviarSaldo();
				comprador = new Comprador(username, saldo);
				lusuarios.add(this.comprador);
				ServerTCP.getListaConexionesLogueados().add(this);
				intentarLogin = false;
			} else {
				pw.println("Login failed");
				pw.flush();
				System.out.println("Enviado: Login failed");
			}
		}else{
			JOptionPane.showMessageDialog(null, "Usuario ya logueado");
			pw.println("Login failed");
			pw.flush();
		}
	}

	public void enviarSaldo() {
		try {
			/*
			 * pw = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()));
			 */
			String sql = "SELECT saldo FROM users WHERE username LIKE '" + username + "';";
			rs = st.executeQuery(sql);
			rs.next();
			saldo = Integer.parseInt(rs.getString(1));
			pw.println(saldo);
			pw.flush();
			System.out.println("Enviado: hola " + username + " tu saldo es: " + saldo);
		} catch (SQLException e) {
			pw.println("conexion failed");
			pw.flush();
			System.out.println("Enviado: conexion failed");
			e.printStackTrace();
		}

	}

	public boolean comprobarLogin(String username, String password) {
		if (comprobarLoginDB("users", "username", username, password)) {
			System.out.println(username + " logueado");
			return true;
		} else {
			return false;
		}

	}

	private boolean comprobarLoginDB(String nomTaula, String column, String username, String password) {
		boolean found = false;
		boolean correcto = false;

		try {
			String sql = "";
			db = DriverManager.getConnection(url, "root", "super3");
			st = db.createStatement();
			String[] types2 = { "TABLE" };
			DatabaseMetaData metadata2 = db.getMetaData();
			ResultSet resultSet2 = metadata2.getTables(null, null, "%", types2);
			while (resultSet2.next()) {

				String tableName = resultSet2.getString(3);

				if (tableName.equals(nomTaula)) {
					// Tabla que ha pedido el usuario
					rs = st.executeQuery("SHOW COLUMNS FROM " + nomTaula);

					int k = 0;
					while (rs.next()) {
						// Obtenemos el numero de columnas de la tabla (k) y los nombres de cada columna
						if (column.equals(rs.getString(1))) {
							k++;
						}
					}
					rs = st.executeQuery("SELECT username, password FROM " + nomTaula);

					while (rs.next()) {
						// obtenemos el valor de cada columna para cada fila
						for (int i = 1; i <= k; i++) {

							if (rs.getString(1).equals(username)) {
								found = true;
								if (rs.getString(2).equals(password)) {
									correcto = true;
								}
							}
						}
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'apertura de connexi�: " + e.getMessage());
		}
		return correcto;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public Comprador getComprador() {
		return comprador;
	}

	public void setComprador(Comprador comprador) {
		this.comprador = comprador;
	}

	public static ArrayList<Comprador> getLusuarios() {
		return lusuarios;
	}

	public static void setLusuarios(ArrayList<Comprador> lusuarios) {
		ConexionServerACliente.lusuarios = lusuarios;
	}

	public BufferedReader getBf() {
		return bf;
	}

	public void setBf(BufferedReader bf) {
		this.bf = bf;
	}

	public PrintWriter getPw() {
		return pw;
	}

	public void setPw(PrintWriter pw) {
		this.pw = pw;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

}
