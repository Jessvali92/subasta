package Clases;
import Cliente.ClientTCP;

public class Comprador {
	
	private int idComprador;
	private String user;
	private String pswd;
	private int saldo;
	private ClientTCP client;

	public Comprador(String user, int saldo) {
		this.user = user;
		this.saldo = saldo;
	}

	public void conectar() {
		//TODO abrir un ClientTCP para este comprador
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public ClientTCP getClient() {
		return client;
	}

	public void setClient(ClientTCP cliente) {
		this.client = cliente;
	}

	public int getIdComprador() {
		return idComprador;
	}

	public void setIdComprador(int idComprador) {
		this.idComprador = idComprador;
	}

}