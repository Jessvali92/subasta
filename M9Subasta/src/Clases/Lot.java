package Clases;

public class Lot {

	private int idLot;
	private int idProducte;
	private int preuSortida;
	private int importAdjudicacio;
	private int idComprador;
	
	public Lot(int idProducte, int preuSortida) {
		this.idProducte=idProducte;
		this.preuSortida=preuSortida;
	}
	
	public int getIdLot() {
		return idLot;
	}
	
	public void setIdLot(int idLot) {
		this.idLot = idLot;
	}
	
	public int getIdProducte() {
		return idProducte;
	}
	
	public void setIdProducte(int idProducte) {
		this.idProducte = idProducte;
	}
	
	public int getPreuSortida() {
		return preuSortida;
	}
	
	public void setPreuSortida(int preuSortida) {
		this.preuSortida = preuSortida;
	}
	
	public int getImportAdjudicacio() {
		return importAdjudicacio;
	}
	
	public void setImportAdjudicacio(int importAdjudicacio) {
		this.importAdjudicacio = importAdjudicacio;
	}
	
	public int getIdComprador() {
		return idComprador;
	}
	
	public void setIdComprador(int idComprador) {
		this.idComprador = idComprador;
	}
}