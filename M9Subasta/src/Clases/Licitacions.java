package Clases;

public class Licitacions {

	private int idLicitacio;
	private int idProducte;
	private String nombreComprador;
	private int importe;
	private boolean acceptada = false;

	public Licitacions(int idProducte, String user, int importe) {
		this.idProducte = idProducte;
		this.nombreComprador = user;
		this.importe = importe;
	}

	public Licitacions() {
		this.importe = 0;
	}

	public int getIdLicitacio() {
		return idLicitacio;
	}

	public void setIdLicitacio(int idLicitacio) {
		this.idLicitacio = idLicitacio;
	}

	public String getNombreComprador() {
		return nombreComprador;
	}

	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}

	public int getImport() {
		return importe;
	}

	public void setImport(int idImport) {
		this.importe = idImport;
	}

	public boolean isAcceptada() {
		return acceptada;
	}

	public void setAcceptada(boolean acceptada) {
		this.acceptada = acceptada;
	}

	public int getIdProducte() {
		return idProducte;
	}

	public void setIdProducte(int idProducte) {
		this.idProducte = idProducte;
	}

}